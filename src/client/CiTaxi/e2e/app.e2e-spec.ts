import { CiTaxiPage } from './app.po';

describe('ci-taxi App', () => {
  let page: CiTaxiPage;

  beforeEach(() => {
    page = new CiTaxiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
