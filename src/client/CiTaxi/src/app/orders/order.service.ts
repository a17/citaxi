import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class OrderService {

  private apiUrl = '/api/orders';

  constructor(
    private http: Http
  ) {}

  placeOrder(order) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(order);

    return this.http.post(this.apiUrl, body, options)
      .map((res: Response) => res);
  }

  listOrders() {
    return this.http.get(this.apiUrl)
      .map((res: Response) => res.json());
  }

}
