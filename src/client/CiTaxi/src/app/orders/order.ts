export class Order {

    constructor(
        public departure: string,
        public destination: string
    ) {}
}