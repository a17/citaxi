import { Component } from '@angular/core';

import { Order } from '../order';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.css'],
  providers: [OrderService]
})
export class OrderFormComponent {

  constructor(
    private orderService: OrderService
  ) {}

  model = new Order("Ermak", "Pavlodar");

  onSubmit() {
    this.orderService.placeOrder(this.model)
      .subscribe(response => {});
  }

}
