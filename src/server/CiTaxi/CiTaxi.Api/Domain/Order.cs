﻿namespace CiTaxi.Api.Domain
{
    public class Order
    {
        public string Departure { get; set; }

        public string Destination { get; set; }
    }
}