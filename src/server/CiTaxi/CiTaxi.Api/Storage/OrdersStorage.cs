﻿using System.Collections.Generic;
using CiTaxi.Api.Domain;

namespace CiTaxi.Api.Storage
{
    public static class OrdersStorage
    {
        public static IList<Order> Orders { get; } = new List<Order>();
    }
}