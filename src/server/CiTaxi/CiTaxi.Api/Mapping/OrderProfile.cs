﻿using AutoMapper;
using CiTaxi.Api.Domain;
using CiTaxi.Api.Models;

namespace CiTaxi.Api.Mapping
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, OrderDto>().ReverseMap();
        }
    }
}