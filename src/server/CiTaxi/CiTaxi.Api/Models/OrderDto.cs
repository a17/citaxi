﻿namespace CiTaxi.Api.Models
{
    public class OrderDto
    {
        public string Departure { get; set; }
        public string Destination { get; set; }
    }
}