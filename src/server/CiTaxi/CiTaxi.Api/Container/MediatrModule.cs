﻿using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Core;
using CiTaxi.Api.Features.Orders;
using CiTaxi.Api.Mediator;
using CiTaxi.Api.Mediator.PreRequestHandlers;
using MediatR;
using Module = Autofac.Module;

namespace CiTaxi.Api.Container
{
    public class MediatrModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MediatR.Mediator>()
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.Register<SingleInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            })
            .InstancePerLifetimeScope();

            var assembly = typeof(Create.Command).GetTypeInfo().Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .As(type => type.GetInterfaces()
                    .Where(interfacetype => interfacetype.IsClosedTypeOf(typeof(IAsyncPreRequestHandler<>))))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(assembly)
                .As(type => type.GetInterfaces()
                    .Where(interfacetype => interfacetype.IsClosedTypeOf(typeof(IAsyncPostRequestHandler<,>))))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(assembly)
                .As(type => type.GetInterfaces()
                    .Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IAsyncRequestHandler<,>)))
                    .Select(interfaceType => new KeyedService("asyncRequestHandler", interfaceType)))
                .InstancePerLifetimeScope();

            builder.RegisterGenericDecorator(typeof(AsyncMediatorPipeline<,>), typeof(IAsyncRequestHandler<,>), "asyncRequestHandler")
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(ValidationPreRequestHandler<>))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}