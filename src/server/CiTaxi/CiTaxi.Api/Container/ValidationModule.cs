﻿using Autofac;
using System.Reflection;
using CiTaxi.Api.Features.Orders;
using FluentValidation;
using Module = Autofac.Module;

namespace CiTaxi.Api.Container
{
    public class ValidationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(Create.CommandValidator).GetTypeInfo().Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.IsClosedTypeOf(typeof(AbstractValidator<>)))
                .AsSelf()
                .InstancePerLifetimeScope();

            builder.RegisterType<Create.CommandValidator>()
                .As<AbstractValidator<Create.Command>>();

            builder.RegisterType<List.QueryValidator>()
                .As<AbstractValidator<List.Query>>();
        }
    }
}