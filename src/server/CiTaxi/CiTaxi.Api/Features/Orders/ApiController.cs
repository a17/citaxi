﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CiTaxi.Api.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CiTaxi.Api.Features.Orders
{
    [Route("api/orders")]
    public class ApiController : Controller
    {
        private readonly IMediator _mediator;

        public ApiController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody]OrderDto order)
        {
            await _mediator.Send(new Create.Command(order));

            return Ok();
        }

        [HttpGet]
        public async Task<IEnumerable<OrderDto>> Get()
        {
            return await _mediator.Send(new List.Query());
        }
    }
}
