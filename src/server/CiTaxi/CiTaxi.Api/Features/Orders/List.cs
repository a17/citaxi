﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CiTaxi.Api.Domain;
using CiTaxi.Api.Models;
using CiTaxi.Api.Storage;
using FluentValidation;
using MediatR;

namespace CiTaxi.Api.Features.Orders
{
    public class List
    {
        public class Query : IRequest<IEnumerable<OrderDto>> { }

        public class QueryValidator : AbstractValidator<Query> { }

        public class QueryHandler : IAsyncRequestHandler<Query, IEnumerable<OrderDto>>
        {
            private readonly IMapper _mapper;

            public QueryHandler(IMapper mapper)
            {
                _mapper = mapper;
            }

            public Task<IEnumerable<OrderDto>> Handle(Query message)
            {
                var orders = OrdersStorage.Orders;

                return Task.FromResult(_mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(orders));
            }
        }
    }
}