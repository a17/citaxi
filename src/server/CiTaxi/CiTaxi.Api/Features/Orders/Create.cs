﻿using System.Threading.Tasks;
using AutoMapper;
using CiTaxi.Api.Domain;
using CiTaxi.Api.Models;
using CiTaxi.Api.Storage;
using MediatR;
using FluentValidation;

namespace CiTaxi.Api.Features.Orders
{
    public class Create
    {
        public class Command : IRequest<Unit>
        {
            public OrderDto OrderDto { get; }

            public Command(OrderDto orderDto)
            {
                OrderDto = orderDto;
            }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(c => c.OrderDto.Departure).NotNull();
                RuleFor(c => c.OrderDto.Destination).NotNull();
            }
        }

        public class CommandHandler : IAsyncRequestHandler<Command, Unit>
        {
            private readonly IMapper _mapper;

            public CommandHandler(IMapper mapper)
            {
                _mapper = mapper;
            }

            public Task<Unit> Handle(Command message)
            {
                var order = _mapper.Map<OrderDto, Order>(message.OrderDto);

                OrdersStorage.Orders.Add(order);

                return Unit.Task;
            }
        }
    }
}