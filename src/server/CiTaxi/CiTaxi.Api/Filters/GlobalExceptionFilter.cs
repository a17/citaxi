﻿using System.Linq;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Server.Kestrel.Internal.Http;

namespace CiTaxi.Api.Filters
{
    public class GlobalExceptionFilter : IExceptionFilter

    {
        public void OnException(ExceptionContext context)
        {
            var validationException = context.Exception as ValidationException;

            if (validationException != null)
            {
                var errors = validationException.Errors.Select(e => new
                {
                    PropertyName = e.PropertyName,
                    ErrorMessage = e.ErrorMessage
                });

                context.Result = new BadRequestObjectResult(errors);
                context.ExceptionHandled = true;
            }
        }
    }
}