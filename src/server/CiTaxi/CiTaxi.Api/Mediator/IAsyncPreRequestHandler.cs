﻿using System.Threading.Tasks;

namespace CiTaxi.Api.Mediator
{
    public interface IAsyncPreRequestHandler<TRequest>
    {
        Task Handle(TRequest request);
    }
}