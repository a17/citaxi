﻿using System.Threading.Tasks;

namespace CiTaxi.Api.Mediator
{
    public interface IAsyncPostRequestHandler<TRequest, TResponse>
    {
        Task Handle(TRequest request, TResponse response);
    }
}